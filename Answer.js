/* Logic
	@param selector   A: $("div"),
									  B: $("img.some_class"),
									  C: $("#some_id"),
									  D: $(".some_class"),
									  E: $("input#some_id"),
									  F: $("div#some_id.some_class"),
									  G: $("div.some_class#some_id")

	@ output an array of dom element(s)


	*******************************************************
	Approach 1
	*******************************************************
	Have a single storage Array that gets

	Use 4 seperate functions
	@func:getByClass       ==>     getElementsByClassName
	@func:getById          ==>     getElementById
	@func:getByTag         ==>     getElementsByTagName
	@func:regDispatcher    ==>     logic code
	@func:matchDomObjects  ==>     return a lis of matchin dom objects


	A pre-porcessor dispatch func
	Use regex to figure it if:
	1) A Tag
	2) An Id
	3) A Class
	4) Error handle incorrect input
	5) Multi-type selectors

	Potentially wrap the functionality in a prototype to make more OO and reusable elsewhere in the future

	Try catch error handling for bad queries
	ATT! Can't return a null or would result in a false positive

	Potential Problem ==> Regex Dispatcher can get messy with lots of different selectors
	Solution          ==> make regexDisp call itself and itterate through 3 If-Else tag,id,class until
											  length of remainder == null/0 !ATT! Ignore this due to CSS selector sequence

	Becuase already in a function I don't see the logic to have a class within it.

	-------------------------END------------------------------

*/


var $ = function (selector) {
	/* TEST AREA
	console.log(selector);
  console.log("s "+selector.split(/(?=[.#])+/));
	*//*
  selector = "div#some_id.some_class";
	alert(document.getElementById('some_id').length);
	console.log(document.getElementById('some_id'));
	alert(document.getElementsByClassName('dsome_class').length);
	console.log(document.getElementsByClassName('some_class'));
	*/

	//global elements
	//stores the dom elements to return
	//careful of false positives
	var elements = [];
  var byIdClass = /(?=[.#])+/;
  var byId = /[#]+/;					//not used here
  var byClass = /[.]+/;				//not used here
  var byTag = /[a-zA-Z0-9]+/; //not used here
  var CssSel = "";
  var ele = [];
  var firstPass = true;


		var Mx3Sort = function(){
			CssSel = selector.split(byIdClass);
			return CssSel; //optional
		};

		// Each of the following 3 methods get by selector and call matchDomObjects if needed.
		var Mx3GetByTag = function(query) {
			if(firstPass == true){
				ele = document.getElementsByTagName(query);
				firstPass = false;
				return ele; //optional
			}else{
				ele = matchDomObjects(document.getElementsByTagName(query));
				return ele; //optional
			}
		};

		var Mx3GetByClass = function(query){
			if(firstPass == true){
				query = query.substr(1);
				ele = document.getElementsByClassName(query);
				firstPass = false;
				return ele; //optional
			}else{
				query = query.substr(1);
				ele = matchDomObjects(document.getElementsByClassName(query));
				return ele; //optional
			}
		};

		var Mx3GetById = function(query) {
			if(firstPass == true){
				query = query.substr(1);
				//this is needed becuase getbyid doesn't return an html collection
				ele.push(document.getElementById(query));
				firstPass = false;
				return ele; //optional
			}else{
				query = query.substr(1);
				//console.log(ele);
				ele = matchDomObjects(document.getElementById(query));

				return ele;//optional
			}
		};


		// reusable
    // @ param 2 arrays or element objects
    // @ returns the matching components in an array
		var matchDomObjects  = function (newArr){
			newEle = [];
			if(newArr == null || ele == null){}
			else{
				if(newArr.length == undefined){
					for(var i = 0; i < ele.length; i++){
						if(ele[i] === newArr){
							newEle.push(newArr);
						}
					}
				}else if (ele.length == undefined) {
					for(var i = 0; i < newArr.length; i++){
						if(ele === newArr[i]){
							newEle.push(ele);
						}
					}
				}else if (ele.length == undefined && newArr == undefined){
						if(ele === newArr){
							newEle.push(ele);
						}
				}else {
					//change thhis to single loop if possible
					for(var i = 0; i < ele.length; i++){
						for(var x = 0; x < newArr.length; x++){
							if(newArr.length >= 0 && ele.length >= 0){
								if(ele[i] === newArr[x]){
									newEle.push(newArr[x]);
								}
							}
						}
					}
				}
		}
			return newEle;
		};


		/*
			CHECK FUNCTIONS --> purpose is in future to make sure that any input is validated
		*/
		var isTag = function(query){
			if(query.charAt(0) != "." && query.charAt(0) != "#")return true;
			else return false;
		};

		var isClass = function(query){
			if(query.charAt(0) == ".")return true;
			else return false;
		};

		var isId = function(query){
			if(query.charAt(0) == "#")return true;
			else return false;
		};


	try {
		selArr = Mx3Sort();
		//console.log(selArr);
		//Bottom to Up to make it more effiecient --> first search by id then class then tags
		for(var itt = 0; itt < selArr.length; itt++){

			if(isTag(selArr[itt])){
				Mx3GetByTag(selArr[itt]);

			}else if(isClass(selArr[itt])){
				Mx3GetByClass(selArr[itt]);

			}else if(isId(selArr[itt])){
				//elements = Mx3GetById(selArr[itt]);
				Mx3GetById(selArr[itt]);
			}
			else{
				elements = "No input ID or class entered";
			}
		}
	} catch (e) {
		console.log(e);

	} finally {
		console.log("Thanks for the opportunity!");
	}

	elements = ele;
	return elements;
}



/*
	Note:: The second approach attempted to build cssSelector object to be able
				 to reuse parts of it like the matchDomObjects func
				 there were problems using this. Perhaps it has something to do with scope.
*/

/*
var $ = function (selector) {
	// TEST AREA
	// console.log(selector);
  //console.log("s "+selector.split(/(?=[.#])+/));


	//global elements
	//stores the dom elements to return
	//careful of false positives
	var elements = [];


	var cssSelector = function(selector) {
    this.selector = selector;
		//alternative is to simply do /[.#]+/ BUT that means that you will always
		//have structure of tag : class : id
		// !!---  In future these methods can be extended to mimic JQUERY selector operations ---!!
		this.byIdClass = /(?=[.#])+/;
    this.byId = /[#]+/;
    this.byClass = /[.]+/;
    this.byTag = /[a-zA-Z0-9]+/;
		this.CssSel = "";
		this.ele = [];
		this.firstPass = true;

		this.Mx3Sort = function(){
			this.CssSel = this.selector.split(this.byIdClass);
			return this.CssSel; //optional
		};

		//Decided to not have them access a global cssSelectors var to make the functions work on their own.
		this.Mx3GetByTag = function(query) {
			if(this.firstPass == true){
				this.ele = document.getElementsByTagName(query);
				this.firstPass == false;
				return this.ele; //optional
			}else{
				this.ele = this.matchDomObjects(document.getElementsByTagName(query, this.ele));
				return this.ele; //optional
			}
		};

		this.Mx3GetByClass = function(query){
			if(this.firstPass == true){
				query = query.substr(1);
				this.ele = document.getElementsByClassName(query);
				this.firstPass = false;
				return this.ele; //optional
			}else{
				query = query.substr(1);
				this.ele = this.matchDomObjects(document.getElementsByClassName(query, this.ele));
				return this.ele; //optional
			}
		};

		this.Mx3GetById = function(query ) {
			if(this.firstPass == true){
				query = query.substr(1);
        //this is needed becuase getbyid doesn't return an html collection
				var idtag = [];
				idtag.push(document.getElementById("some_id"));
				this.ele = idtag;
				this.firstPass = false;
				return this.ele; //optional
			}else{
				alert("s");
				query = query.substr(1);
				this.ele = this.matchDomObjects(document.getElementById(query, this.ele));
				return this.ele;//optional
			}
		};

    // reusable
    // @ param 2 arrays or element objects
    // @ returns the matching components in an array
		this.matchDomObjects  = function (newArr, ele){
      matchedComponents = [];
			if(newArr == null || ele == null);
			else{
				if(newArr.length == undefined){
					for(var i = 0; i < length; i++){
						if(ele[i] === newArr){
							matchedComponents.push(newArr);
						}
					}
				}else if (ele.length == undefined) {
					for(var i = 0; i < newArr.length; i++){
						if(ele === newArr[i]){
							matchedComponents.push(ele);
						}
					}
				}else if (ele.length == undefined && newArr == undefined){
						if(ele === newArr){
							matchedComponents.push(ele);
						}
				}else {
					for(var i = 0; i < ele.length; i++){
						for(var x = 0; x < newArr.length; x++){
							if(newArr.length >= 0 && ele.length >= 0){
								if(ele[i] === newArr[x]){
									matchedComponents.push(newArr[x]);
								}
							}
						}
					}
				}
		  }
			return matchedComponents;
		};



		//	CHECK FUNCTIONS ||
		//
		this.isTag = function(query){
			if(query.charAt(0) != "." && query.charAt(0) != "#")return true;
			else return false;
		};

		this.isClass = function(query){
			if(query.charAt(0) == ".")return true;
			else return false;
		};

		this.isId = function(query){
			if(query.charAt(0) == "#")return true;
			else return false;
		};
	}



	var regDisp = new cssSelector(selector);
	try {
		selArr = regDisp.Mx3Sort();
		//Bottom to Up to make it more effiecient --> first search by id then class then tags
		for(var itt = 0; itt < selArr.length ; itt++){

			if(regDisp.isTag(selArr[itt])){
				regDisp.Mx3GetByTag(selArr[itt]);

			}else if(regDisp.isClass(selArr[itt])){
				regDisp.Mx3GetByClass(selArr[itt]);

			}else if (regDisp.isId(selArr[itt])){
				regDisp.Mx3GetById(selArr[itt]);
			}
			else{
				elements = "No input ID or class entered";
			}
		}
	} catch (e) {
		console.log(e);

	} finally {
		// do nothing
	}

	elements = regDisp.ele;
	return elements;
}*/
